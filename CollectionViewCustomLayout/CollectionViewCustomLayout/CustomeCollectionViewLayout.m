//
//  CustomeCollectionViewLayout.m
//  CollectionViewCustomLayout
//
//  Created by Mr.LuDashi on 15/9/15.
//  Copyright (c) 2015年 ZeluLi. All rights reserved.
//

#import "CustomeCollectionViewLayout.h"
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

@interface CustomeCollectionViewLayout ()

//section的数量
@property (nonatomic) NSInteger numberOfSections;

//section中Cell的数量
@property (nonatomic) NSInteger numberOfCellsInSections;

//瀑布流的行数
@property (nonatomic) NSInteger columnCount;

//cell边距
@property (nonatomic) NSInteger padding;

//cell的最小高度
@property (nonatomic) NSInteger cellMinHeight;

//cell的最大高度，最大高度比最小高度小，以最小高度为准
@property (nonatomic) NSInteger cellMaxHeight;

//cell的宽度
@property (nonatomic) CGFloat cellWidth;

//存储每個Cell
@property (strong, nonatomic) NSMutableArray *cellArray;

//存储每列Cell的X坐标
@property (strong, nonatomic) NSMutableArray *cellXArray;

//记录每列Cell的最新Cell的Y坐标
@property (strong, nonatomic) NSMutableArray *cellYArray;

@end

@implementation CustomeCollectionViewLayout

#pragma mark -- <UICollectionViewLayout>虚基类中重写的方法

- (void)prepareLayout{
    [super prepareLayout];
    
    [self initData];
    [self initCellWidth];
}

- (CGSize)collectionViewContentSize{
    
    CGFloat height = [self maxCellYArrayWithArray:_cellYArray];
    return CGSizeMake(SCREEN_WIDTH,  height);
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    
    [self initCellYArray];
    NSMutableArray *array = [NSMutableArray array];
    
    //add cells
    for (int i=0; i < _numberOfCellsInSections; i++)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        [array addObject:attributes];
    }
    
    return array;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    NSInteger cellType = [[_cellArray[indexPath.row] objectForKey:@"type"] integerValue];
    CGRect frame = CGRectZero;
    CGFloat cellWidth = [[_cellArray[indexPath.row] objectForKey:@"width"] floatValue];
    CGFloat cellHeight = [[_cellArray[indexPath.row] objectForKey:@"height"] floatValue];
    NSInteger minYIndex = [self minCellYArrayWithArray:_cellYArray type:cellType];
    NSInteger realMinYIndex = [self minCellYArrayWithArray:_cellYArray type:TypeNone];
    CGFloat tempX = [_cellXArray[minYIndex] floatValue];
    CGFloat tempY = [_cellYArray[minYIndex] floatValue];
    CGFloat minY = [_cellYArray[realMinYIndex] floatValue];
    BOOL resized = NO;
    
    //NSLog(@"Cell%ld\nY:%f,%f,%f", (long)indexPath.row, [_cellYArray[0] floatValue], [_cellYArray[1] floatValue], [_cellYArray[2] floatValue]);
    
    if (cellType == TypeBig) {
        // fill up left when big cell at right side
        if (minYIndex == 0 && ([self roundFloat:[_cellYArray[0] floatValue]] < [self roundFloat:[_cellYArray[1] floatValue]])) {
            //resize cell to small
            resized = YES;
            NSMutableDictionary *currentCell = [_cellArray objectAtIndex:indexPath.row];
            [self setCell:currentCell type:TypeSmall];
            cellWidth = [[_cellArray[indexPath.row] objectForKey:@"width"] floatValue];
            cellHeight = [[_cellArray[indexPath.row] objectForKey:@"height"] floatValue];
        }
        
        // fill up with small cell when too many big cell
        if (!resized && (tempY + cellHeight - minY) > (cellHeight *2)) {
            //resize cell to small
            NSMutableDictionary *currentCell = [_cellArray objectAtIndex:indexPath.row];
            [self setCell:currentCell type:TypeSmall];
            cellWidth = [[_cellArray[indexPath.row] objectForKey:@"width"] floatValue];
            cellHeight = [[_cellArray[indexPath.row] objectForKey:@"height"] floatValue];
            
            //recount cell location 
            minYIndex = [self minCellYArrayWithArray:_cellYArray type:TypeSmall];
            tempX = [_cellXArray[minYIndex] floatValue];
            tempY = [_cellYArray[minYIndex] floatValue];
        }
    }
    
    // update Y position of each column
    if (cellType == TypeBig && minYIndex < (_columnCount -1)) {
        _cellYArray[minYIndex+1] = @([_cellYArray[minYIndex] floatValue] +cellHeight +_padding);
    }
    _cellYArray[minYIndex] = @(tempY +cellHeight +_padding);
    
    //set frame
    frame = CGRectMake(tempX, tempY, cellWidth, cellHeight);
    attributes.frame = frame;
    
    return attributes;
}

- (void) initData{
    _numberOfSections = [self.collectionView numberOfSections];
    _numberOfCellsInSections = [self.collectionView numberOfItemsInSection:0];
    _columnCount = 3;
    _padding = 2;
    _cellMinHeight = 50;
    _cellMaxHeight = 200;
    
    _cellArray = [[NSMutableArray alloc] initWithCapacity:_numberOfCellsInSections];
    _cellWidth = (SCREEN_WIDTH - (_columnCount -1) * _padding) / _columnCount;
    for (int i = 0; i < _numberOfCellsInSections; i ++) {
    
        NSMutableDictionary *cellInfo = [NSMutableDictionary dictionary];
        if ((arc4random() % 3) > 0) {
            [self setCell:cellInfo type:TypeSmall];
        } else {
            [self setCell:cellInfo type:TypeBig];
        }
        [_cellArray addObject: cellInfo];
    }
}

- (void)setCell:(NSMutableDictionary *)cell type:(NSInteger)typeToSet {
    
    if (typeToSet == TypeBig) {
        [cell setObject:[NSNumber numberWithInteger:TypeBig] forKey:@"type"];
        [cell setObject:@(_cellWidth *2 +_padding) forKey:@"width"];
        [cell setObject:@(_cellWidth *2 +_padding) forKey:@"height"];
    } else if (typeToSet == TypeSmall) {
        [cell setObject:[NSNumber numberWithInteger:TypeSmall] forKey:@"type"];
        [cell setObject:@(_cellWidth) forKey:@"width"];
        [cell setObject:@(_cellWidth) forKey:@"height"];
    }
}

- (void) initCellWidth{
    _cellWidth = (SCREEN_WIDTH - (_columnCount -1) * _padding) / _columnCount;
    _cellXArray = [[NSMutableArray alloc] initWithCapacity:_columnCount];
    
    for (int i = 0; i < _columnCount; i ++) {
        CGFloat tempX = i * (_cellWidth + _padding);
        [_cellXArray addObject:@(tempX)];
    }
}

- (void) initCellYArray{
    _cellYArray = [[NSMutableArray alloc] initWithCapacity:_columnCount];
    
    for (int i = 0; i < _columnCount; i ++) {
        [_cellYArray addObject:@(0)];
    }
}

- (CGFloat) maxCellYArrayWithArray: (NSMutableArray *) array{
    if (array.count == 0) {
        return 0.0f;
    }
    
    CGFloat max = [array[0] floatValue];
    for (NSNumber *number in array) {
        
        CGFloat temp = [number floatValue];
        if (max < temp) {
            max = temp;
        }
    }
    
    return max;
}

- (CGFloat) minCellYArrayWithArray: (NSMutableArray *) array type:(NSInteger)type {
    
    if (array.count == 0) {
        return 0.0f;
    }
    
    NSInteger minIndex = 0;
    int roundMin = [self roundFloat:[array[0] floatValue]];
    long searchColumn;
    if (type == TypeBig) {
        searchColumn = _columnCount -1;
    } else {
        searchColumn = _columnCount;
    }
    
    for (int i = 0; i < searchColumn; i ++) {
        int roundTemp = [self roundFloat:[array[i] floatValue]];
        
        if (roundMin > roundTemp) {
            roundMin = roundTemp;
            minIndex = i;
        }
    }
    
    return minIndex;
}

- (int) roundFloat:(CGFloat)floatValue {

    return (int)(floatValue +0.5);
}

@end
