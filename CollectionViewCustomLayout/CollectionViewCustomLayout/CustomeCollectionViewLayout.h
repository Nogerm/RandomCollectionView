//
//  CustomeCollectionViewLayout.h
//  CollectionViewCustomLayout
//
//  Created by Mr.LuDashi on 15/9/15.
//  Copyright (c) 2015年 ZeluLi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DataSourceType) {
    TypeBig,
    TypeSmall,
    TypeNone
};

@interface CustomeCollectionViewLayout : UICollectionViewLayout

@end
