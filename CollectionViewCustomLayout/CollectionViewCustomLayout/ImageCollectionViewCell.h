//
//  ImageCollectionViewCell.h
//  CollectionViewCustomLayout
//
//  Created by Mr.LuDashi on 15/9/21.
//  Copyright (c) 2015年 ZeluLi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "KASlideShow.h"

@interface ImageCollectionViewCell : UICollectionViewCell<KASlideShowDelegate>

@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UILabel *cellTextView;
@property (strong, nonatomic) KASlideShow *slideShow;
@property (strong, nonatomic) NSMutableArray *imageUrlArray;

- (void) initAnim;

@end
