//
//  ImageCollectionViewCell.m
//  CollectionViewCustomLayout
//
//  Created by Mr.LuDashi on 15/9/21.
//  Copyright (c) 2015年 ZeluLi. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (void) initAnim {
    
    _imageUrlArray = [NSMutableArray array];
    switch ((arc4random() % 4)) {
        case 0:
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/golf1.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/golf2.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/golf3.png"];
            break;
            
        case 1:
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/badminton1.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/badminton2.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/badminton3.png"];
            break;
            
        case 2:
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/bike1.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/bike2.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/bike3.png"];
            break;
            
        case 3:
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/basketball1.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/basketball2.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/basketball3.png"];
            break;
            
        case 4:
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/fish1.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/fish2.png"];
            [_imageUrlArray addObject:@"https://witch.blob.core.windows.net/thumbnail/fish3.png"];
            break;
            
        default:
            break;
    }
    
    _slideShow = [[KASlideShow alloc] initWithFrame:CGRectMake(0,0,220,220)];
    [_slideShow setDelay:(arc4random() % 3) +5]; // Delay between transitions
    [_slideShow setTransitionDuration:1]; // Transition duration
    [_slideShow setTransitionType:KASlideShowTransitionSlideVertical]; // Choose a transition type (fade or slide)
    [_slideShow setImagesContentMode:UIViewContentModeScaleAspectFill]; // Choose a content mode for images to display
    _slideShow.delegate = self;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    for (NSURL *snap in _imageUrlArray) {
        [manager downloadImageWithURL:snap options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image)
            {
                [_slideShow addImage:image];
            }
        }];
    }
    
    [_cellView addSubview:_slideShow];
    [_slideShow start];
}

@end
